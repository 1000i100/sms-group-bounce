# coSMoS

Serveur sms de communication groupée.

*Note : pour l'instant il s'agit d'une note d'intension, rien n'est réalisé.*

## Mise en place administrateur
- disposer ou mettre en place un ordinateur sous linux allumé en permanance.
- disposer ou acquérir un module gsm, 3G ou 4G (dongle, clef, modem routeur...)
- disposer ou acquérir une carte sim associé à un forfait sms illimité. Le numéro associé à cette carte sim sera, pour simplifié, désigné comme étant `06SRV00000`.
- installer Cosmos et peutêtre le configurer.

## Utilisation
Pour bénéficier des services de diffusion groupé de Cosmos, chaque sms est à envoyer au numéro du serveur : `06SRV00000` dans notre cas.

Vous pouvez utiliser deux type de groupes.
- Des groupes només, où les message de chaque participant sont envoyé à tous les autres participant du groupe. Syntaxe des sms envoyable au `06SRV00000` :
    - [ ] `G xxxx` s'ajoute (et cré à la volé si nécessaire) au groupe `xxxx`. (vous permet de le rejoindre)
    - [ ] `G xxxx mon message` envoi `mon message` à tout les membre du groupe `xxxx`
    - [ ] `Q xxxx` permet de quitter le groupe `xxxx` (ne plus en recevoir les messages de ce groupe)
- Pour chaque usager, un groupe personnel. Si vous envoyer un message quelquonque au `06SRV00000`, il sera envoyé au inscrit à votre groupe personnel.
    - [ ] `mon message` envoi `mon message` à tout les membres de son groupe personnel (associé à son numéro de téléphone)
    - [ ] `A 06DESxxxxx` ajoute/invite le destinataire au numéro `06DESxxxxx` à votre groupe personnel.
    - [ ] `OK 06PERSOxxx` pour valider une invitation d'ajout au groupe personnel de `06PERSOxxx`.

Pour chaque compte, indépendament des groupes, quelques commandes sont disponnibles :
- [ ] `STOP` pour ne plus jamais recevoir de message de ce serveur (sauf à reconacter le serveur ulterieurement avec une autre instruction que `STOP`)
- [ ] `C signature` permet d'ajouter au suffixe de ses propres sms la `signature` définie. Exemple : au lieu de recevoir des sms finissant par ` // envoyé par 06xxxxxxxx` vos destinataires recevrons des messages finissant par ` // envoyé par 06xxxxxxxx signature` donc après `C Bob` vos destinataires auront en fin de messages ` // envoyé par 06xxxxxxxx Bob`. Syntaxe alternatives : C(contact) N(nom) ou S(signature) suivi du contenu de la signature.
- [ ] `R code modele` défini (ou remplace) le raccorci associé à `code`. Il sera remplacé par `modele` lors des usages ulterieurs. Exemple : `06MOIxxxxx` envoi le sms `R 1 SOS` au `06SRV00000`. Il peut ensuite envoyer simplement `1` (ou `G BDX 1`) pour que les destinataire de son groupe reçoivent : `SOS // envoyé par 06MOIxxxxx`. Syntaxe alternatives : R(accourci) T(emplate) ou M(odèle) suivi de la référence puis de son contenu de remplacement.
- [ ] `R code` supprime le raccourci/modèle/template associé au `code`.

Au fur et à mesure du développement je cocherais les cases pour indiquer les fonctions disponnibles.

Si vous souhaitez d'autres fonctionnalités ou rencontré des bugs, je vous invite à m'en faire part, après avoir vérifié qu'un autre ticket n'existe pas déjà à ce sujet.
Pour m'en faire part, ouvrez un ticket : https://framagit.org/1000i100/sms-group-bounce/issues


## Soutiens :

Je passe mon temps à faire ce qui à le plus de sens à mes yeux.
Souvent il s'agit de créer des outils libre comme celui-ci.
Parfois, il s'agit d'assurer ma survie en vendant mon temps de vie au plus offrant.

Vous pouvez, à votre mesure, participer à faire que ce qui fasse sens pour moi corresponde à construire un monde meilleur pour tous plutôt qu'assurer ma survie.
Pour cela, je vous donne rendez-vous sur une de ces plateformes de soutien :

- https://liberapay.com/1000i100/
